#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/gnn_gamma_IL
#
#*********************************************************************************

import os
import random
from copy import deepcopy
from datetime import datetime
from time import time
import shutil
import numpy as np
import yaml
import argparse
import pandas as pd
import sklearn.metrics

import torch
import torch.nn as nn
import wandb
from torch_geometric.loader import DataLoader

from Data.graph_IL import GraphILDataset
from model.matrix_completion_networks import MCM_MLP
from model.graph_networks import GNN
from utils.split_dataset import split_train_vali
from utils.calculate_metrics import calculate_result_metrics

# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def infer(config_path=None, dataloader=None):
    
    with open(config_path) as config_yaml:
        config_dict = yaml.safe_load(config_yaml)
        for key, value in config_dict.items():
            if isinstance(value, dict):
                config_dict[key] = value
    
    print(f"\n---> Start init of model: {config_dict['model_id']}.")
    if config_dict["model_id"] in ["GraphNN", "GNN"]:
        model = GNN(**config_dict).to(device)
    elif 'MCM_MLP' in config_dict["model_id"]:
        data_set = GraphILDataset(f"./Data/IL/Train")
        model = MCM_MLP(data_set, **config_dict).to(device)
    
    model.load_state_dict(torch.load(config_dict["save_model_path"], map_location=torch.device(device)))
    print(f"\tLoaded model parameters from {config_dict['save_model_path']}")
    print("---> Finished init of model.")

    def infer(loader):        
        model.eval()
        predictions = torch.tensor([], device=device)
        real_values = torch.tensor([], device=device)
        # get all predictions
        for data in loader:
            data = data.to(device)
            predictions = torch.cat((predictions, model(data).view(-1).detach()))
            real_values = torch.cat((real_values, data.y.detach()))
            
        return predictions, real_values
    
    predictions, real_values = infer(dataloader)
    return predictions.cpu().detach().numpy(), real_values.cpu().detach().numpy()

    
def evaluate_ensemble(ensemble_paths, dataloader, df_data_set):
        
    # predictions from each ensemble
    all_predictions = []
    
    # evaulate each model indivudally and store results
    for m_idx, m_p in enumerate(ensemble_paths):
        print(f"\nEvaulate model #: {m_p}")
        # infer 
        if dataloader is not None:
            predictions, _ = infer(config_path=m_p, dataloader=dataloader)
        else: raise ValueError("No dataloader or dataset provided.")
        # store model output 
        all_predictions.append(predictions)
    mean_predictions = np.mean(np.array(all_predictions), axis=0)
    df_data_set[f"predicted_lngamma_Ensemble"] = mean_predictions

    return df_data_set


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    # ./Data/IL/tmp_folder/raw/raw.csv
    parser.add_argument('--predict_data_path', default="./Data/Predict/example.csv", type=str)
    # GNN model
    parser.add_argument('--model_type', default="GNN", type=str)
    # model trained for prediction test (for generalization test, choose "IL_generalization")
    parser.add_argument('--data', default="IL", type=str)
    args = parser.parse_args()

    tmp_data_path = f"./Data/{args.data}/tmp_folder/raw/raw.csv"
    df_data_set = pd.read_csv(args.predict_data_path)
    # add T-normalization
    df_data_set["normalized_T"] = (df_data_set["Temperature"].to_numpy() - 288.15) / (448.15 - 288.15)
    # add default columns
    df_data_set[['solute_family_id_x', 'index', 'IL_id', 'cation_id', 'lngamma', 'cationic_family_id', 'solute_id', 'T/K', 'anion_id']] = None
    # save data to tmp folder for processing
    os.makedirs(os.path.dirname(tmp_data_path), exist_ok=True)
    df_data_set.to_csv(tmp_data_path, sep=";", index=False)
    
    # delete old processed data from tmp
    if os.path.isdir(f"./Data/{args.data}/tmp_folder/processed"):
        shutil.rmtree(f"./Data/{args.data}/tmp_folder/processed")
    
    df_data_set = pd.read_csv(tmp_data_path, sep=";")
    data_set = GraphILDataset(os.path.dirname(os.path.dirname(tmp_data_path)))
    # remove default columns
    print(df_data_set)
    df_data_set = df_data_set.drop(columns=['solute_family_id_x', 'index', 'IL_id', 'cation_id', 'lngamma', 'cationic_family_id', 'solute_id', 'T/K', 'anion_id'])
    data_loader = DataLoader(data_set, batch_size=1024, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)

    # paths for models in ensemble
    ensemble_paths = [f"./results/trained_models/{args.data}/{args.model_type}/{path}" for path in os.listdir(f"./results/trained_models/{args.data}/{args.model_type}/") if "config" in path]
    df_test_set = evaluate_ensemble(ensemble_paths, data_loader, df_data_set)

    print(df_test_set)
    df_test_set.to_csv(f"{os.path.dirname(args.predict_data_path)}/predictions.csv")

    