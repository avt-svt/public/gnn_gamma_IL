#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/gnn_gamma_IL
#
#*********************************************************************************

import os
import random
from copy import deepcopy
from datetime import datetime
from time import time
import numpy as np
import pandas as pd
import yaml

import argparse
import torch
import torch.nn as nn
from torch_geometric.loader import DataLoader

from Data.graph_IL import GraphILDataset
from model.matrix_completion_networks import MCM_MLP
from model.graph_networks import GNN
from utils.split_dataset import generate_sets, generate_generalization_sets, split_train_vali
from utils.calculate_metrics import calculate_result_metrics

from argparse import Namespace
import time
from utils import EarlyStopping

# Ignore warnings
import warnings
warnings.filterwarnings("ignore")


parser = argparse.ArgumentParser()

# Network parameters
parser.add_argument('--model_id', default="GNN", type=str)
# GNN parameters
parser.add_argument('--share_embedding', default=False, type=bool)
parser.add_argument('--num_node_features', default=22, type=int)
parser.add_argument('--num_edge_features', default=6, type=int)
parser.add_argument('--conv_type', default="GINEConv", type=str)
parser.add_argument('--num_convs', default=2, type=int)
parser.add_argument('--use_gru', default=True, type=bool)
parser.add_argument('--dim_fingerprint', default=64, type=int)
parser.add_argument('--pool_type', default="add", type=str)
parser.add_argument('--num_channel_layers', default=2, type=int)
parser.add_argument('--dim_channel', default=256, type=int)
parser.add_argument('--interaction_function', default="cat", type=str)
parser.add_argument('--dim_interaction_layers', default=128, type=int)
parser.add_argument('--dropout_p1', default=0.0, type=float)
parser.add_argument('--activation', default="LeakyReLU", type=str)
parser.add_argument('--multi_gru', default=False, type=bool)

# MCM parameters
parser.add_argument('--dim_hidden_channel', default=128, type=int)
parser.add_argument('--dim_interaction', default=192, type=int)
parser.add_argument('--dropout_hidden', default=0.05, type=float)
parser.add_argument('--dropout_interaction', default=0.03, type=float)
parser.add_argument('--descriptors', default=True, type=bool)

# Training parameters
parser.add_argument('--lr', default=0.001, type=float)
parser.add_argument('--lrfactor', default=0.8, type=float)
parser.add_argument('--lrpatience', default=3, type=int)
parser.add_argument('--batch_size', default=64, type=int)
parser.add_argument('--epochs', default=1, type=int)
parser.add_argument('--optim', default="adam", type=str)
parser.add_argument('--esp', default=25, type=int)

# Data, split, and logs
parser.add_argument('--rs', default=42, type=int)
parser.add_argument('--split', default=0, type=int)
parser.add_argument('--data', default="IL", type=str)

# Note: used seeds
#[42, 85, 123, 456, 581, 1959, 8639, 9124, 5471, 32925, 24, 94, 392, 454, 831, 8342, 7583, 1324, 3888, 94524, 93333, 93271, 37223, 56273, 93255, 89344, 45183, 66122, 38393, 97439, 423943, 482331, 381315, 958331, 938653, 753839, 832912, 842932, 854666, 134843]

hyperparameter_defaults = parser.parse_args()

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def seed_everything(seed: int):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True
    

def trainer(config=None):

    date_time = datetime.now().strftime("%m-%d-%Y-%H-%M-%S")
    save_path = f"{config.model_id}_{config.rs}_{date_time}_{str(datetime.now().microsecond)}"

    save_path_config = f"./results/trained_models/{config.data}/{config.model_id}/{save_path}"
    save_path = f"{save_path_config}.pt"
    config_dict = vars(config)
    config_dict["save_model_path"] = save_path
    with open(f'{save_path_config}_config.yml', 'w') as cf:
        yaml.dump(config_dict, cf)

    # set seed
    seed_everything(config.rs)
    print(f"Set seed to: {config.rs}.")
    
    # load dataset
    print(f"\n---> Start loading of dataset {config.data} and data preprocessing.")
    if config.data == "IL":
        generate_sets()
        data_set = GraphILDataset(f"./Data/IL/Train").shuffle()
        train_set, vali_set = split_train_vali(data_set, config.split) 
        test_set = GraphILDataset(f"./Data/IL/Test").shuffle()
    elif config.data == "IL_generalization":
        generate_generalization_sets(seed=config.rs)
        train_set = GraphILDataset(f"./Data/IL_generalization/Train_{config.rs}").shuffle()
        vali_set = GraphILDataset(f"./Data/IL_generalization/Vali_{config.rs}").shuffle()
        test_set = GraphILDataset(f"./Data/IL_generalization/Test").shuffle()
    else:
        raise ValueError(f"Dataset {config.data} not available.")
    
    num_node_features = train_set.data.x_i[0].shape[0]
    num_edge_features = train_set.data.edge_attr_i[0].shape[0]
    config_dict["num_node_features"] = num_node_features
    config_dict["num_edge_features"] = num_edge_features

    # Initialize data loaders
    trainloader = DataLoader(train_set, batch_size=config.batch_size, shuffle=True, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True, worker_init_fn=config.rs)
    valiloader = DataLoader(vali_set, batch_size=config.batch_size, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)
    testloader = DataLoader(test_set, batch_size=config.batch_size, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)
        
    if 'GNN' in config.model_id: 
        print('\tTraining is based on ' + str(num_node_features) + ' atom features and ' + str(num_edge_features) + ' edge features for a molecule.')
    print(f"\tTraining data #: {len(train_set)}, Validation data #: {len(vali_set)}, Test set #: {len(test_set)}") 
    print('---> Finished loading of dataset and data preprocessing.')
    
    print(f"\n---> Start init of model: {config.model_id}.")
    print(f"Model config: {config_dict}")
    if config.model_id == "GNN":
        model = GNN(**config_dict).to(device)
    elif config.model_id == 'MCM_MLP':
        model = MCM_MLP(data_set, **config_dict).to(device)
    print(f"---> Finished init of {config.model_id} model.")
    print(model)

    # Define the loss function and optimizer
    loss_function = nn.MSELoss()
    if config.optim == "sgd":
        optimizer = torch.optim.SGD(model.parameters(), lr=config.lr, momentum=0.9)
    elif config.optim == "adam":
        optimizer = torch.optim.Adam(model.parameters(), lr=config.lr)

    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, factor=config.lrfactor, patience=config.lrpatience, min_lr=0.0000001)

    early_stopping = EarlyStopping(patience=config.esp, verbose=True, save_path=save_path)
    
    # Training
    def train(loader):
        model.train()
        loss_all = 0

        for data in loader:
            data = data.to(device)
            optimizer.zero_grad()
            tmp_pred_data = model(data).view(-1)
            tmp_real_data = data.y
            loss = loss_function(tmp_pred_data, tmp_real_data)
            loss.backward()
            optimizer.step()
            loss_all += loss.item() * data.num_graphs

        return loss_all / len(loader.dataset)

    # Testing
    def validate(loader):
        model.eval()
        std_pred_error = 0
        rel_pred_error = 0
        mae, mre, mae_real = 0, 0, 0
        loss_all = 0
        for data in loader:
            data = data.to(device)
            tmp_pred_data = model(data).view(-1).detach()
            tmp_real_data = data.y
            loss = loss_function(tmp_pred_data, tmp_real_data)
            loss_all += loss.item() * data.num_graphs

            # Error calculations
            mae_real += (torch.exp(tmp_pred_data)-torch.exp(tmp_real_data)).abs().sum(0).item() # y is ln
            std_pred_error += (tmp_pred_data - tmp_real_data).abs().sum(0).item()
            rel_pred_error += ((tmp_pred_data - tmp_real_data)/(tmp_real_data)).abs().sum(0).item()

        mae = std_pred_error
        mre = rel_pred_error

        return loss_all / len(loader.dataset), loss_all**2 / len(loader.dataset), mae / len(loader.dataset), mae_real / len(loader.dataset)
    
    # Final testing
    def validation_metrics(loader):
        model.eval()
        predictions = torch.tensor([], device=device)
        real_values = torch.tensor([], device=device)
        # get all predictions
        for data in loader:
            data = data.to(device)
            predictions = torch.cat((predictions, model(data).view(-1).detach()))
            real_values = torch.cat((real_values, data.y))
            
        # mae, mape for ln values
        ln_results = calculate_result_metrics(y_true=real_values, y_pred=predictions)
        # not ln values
        exp_predictions = torch.exp(predictions)
        exp_real_values = torch.exp(real_values) 
        results = calculate_result_metrics(y_true=exp_real_values, y_pred=exp_predictions)
            
        return ln_results, results

  
    print(f"\n---> Start training with max number of epochs: {config.epochs}")
    best_Validation_MAE = None
    for epoch in range(1, config.epochs+1):

        lr = scheduler.optimizer.param_groups[0]['lr']
        loss = train(trainloader)
        train_loss, train_mse, train_mae, train_mae_real = validate(trainloader)
        vali_loss, vali_mse, vali_mae, vali_mae_real  = validate(valiloader)
        test_loss, test_mse, test_mae, test_mae_real = validate(testloader)

        scheduler.step(vali_mae)

        print(
        '\n\tEpoch: {:03d}, LR: {:4f}, Train MAE: {:.4f}, Train MAE real {:.2f}, Vali MAE: {:.4f}, Vali MAE real {:.2f}, Test MAE: {:.4f}, Test MAE real {:.2f}'
        .format(epoch, lr, train_mae, train_mae_real, vali_mae, vali_mae_real, test_mae, test_mae_real))


        if best_Validation_MAE is None:
            best_epoch = epoch
            best_Validation_MAE, best_epoch_Test_MAE, best_epoch_Train_MAE, best_Validation_MAE_real, best_epoch_Test_MAE_real, best_epoch_Train_MAE_real = vali_mae, test_mae, train_mae, vali_mae_real, test_mae_real, train_mae_real

        elif vali_mae < best_Validation_MAE:
            best_epoch = epoch
            best_Validation_MAE, best_epoch_Test_MAE, best_epoch_Train_MAE, best_Validation_MAE_real, best_epoch_Test_MAE_real, best_epoch_Train_MAE_real = vali_mae, test_mae, train_mae, vali_mae_real, test_mae_real, train_mae_real

        early_stopping(vali_mae, model)

        if early_stopping.early_stop or epoch == config.epochs:
            print("Stopping")
            print("Calculating final results...")
            model.load_state_dict(torch.load(save_path))
            if config.data == "IL":
                test_data_df = pd.read_csv(f"Data/IL/Test/raw/raw.csv", sep=";")
            elif config.data == "IL_generalization":        
                test_data_df = pd.read_csv(f"Data/IL_generalization/Test/raw/raw.csv", sep=";")
            else:
                raise ValueError(f"Dataset {config.data} not available.")
            
            train_ln_results, train_results = validation_metrics(trainloader)
            vali_ln_results, vali_results = validation_metrics(valiloader)
            test_ln_results, test_results = validation_metrics(testloader)
            
            print(f"\nFinal Train ln(g): \n{train_ln_results}")
            print(f"\nFinal Train g: \n{train_results}")
            print(f"\nFinal Vali ln(g): \n{vali_ln_results}")
            print(f"\nFinal Vali g: \n{vali_results}")
            print(f"\nFinal Test ln(g): \n{test_ln_results}")
            print(f"\nFinal Test g: \n{test_results}")

            break

    # Process is complete.
    print('---> Finished training.')

if __name__ == '__main__':

    config = hyperparameter_defaults

    start_t = time.time()
    trainer(config)
    end_t = time.time()
    print(f"\nExecution time: {(end_t - start_t) / 60} mins")
