#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/gnn_gamma_IL
#
#*********************************************************************************

import torch
import torch.nn as nn
import torch.nn.functional as F


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def get_mlp_module(dim_in, dim_hidden, dropout):
    mlp_module_list = nn.ModuleList()
    mlp_module_list.append(
        nn.Sequential(
            nn.Embedding(dim_in, dim_hidden),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Linear(dim_hidden, dim_hidden),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Linear(dim_hidden, dim_hidden),
            nn.ReLU(),
            nn.Dropout(dropout),
            nn.Linear(dim_hidden, dim_hidden),
            nn.ReLU(),
        )
    )
    return mlp_module_list

class MCM_MLP(nn.Module):

    def __init__(self, dataset, dim_hidden_channels=128, dim_interaction=192, dropout_hidden=0.05, dropout_interaction=0.03, descriptors=False, **kwargs):
        super().__init__()
        
        self.descriptors = descriptors

        self.dropout_p1 = dropout_hidden
        self.dropout_p2 = dropout_interaction
    
        self.dim_interaction = dim_interaction
        self.dim_hidden_channels = dim_hidden_channels

        self.IL_emb = get_mlp_module(int((dataset.data.id_i.max() + 1).item()), self.dim_hidden_channels, self.dropout_p1)
        self.s_emb = get_mlp_module(int((dataset.data.id_s.max() + 1).item()), self.dim_hidden_channels, self.dropout_p1)
        mid_emb = 2 * self.dim_hidden_channels
        if self.descriptors:
            self.cation_emb = get_mlp_module(int((dataset.data.id_cation.max() + 1).item()), self.dim_hidden_channels, self.dropout_p1)
            self.anion_emb = get_mlp_module(int((dataset.data.id_anion.max() + 1).item()), self.dim_hidden_channels, self.dropout_p1)
            self.cationic_family_emb = get_mlp_module(int((dataset.data.id_cationic_family.max() + 1).item()), self.dim_hidden_channels, self.dropout_p1)
            self.solute_family_emb = get_mlp_module(int((dataset.data.id_solute_family.max() + 1).item()), self.dim_hidden_channels, self.dropout_p1)
            mid_emb = 6 * self.dim_hidden_channels
        
        
        self.layers_middel = nn.Sequential(
            nn.Dropout(self.dropout_p2),
            nn.Linear(mid_emb, 384),
            nn.ReLU(),
            nn.Dropout(self.dropout_p2),
            nn.Linear(384, self.dim_interaction),
            nn.ReLU(),
            nn.Dropout(self.dropout_p2),
        )
        self.layers_end = nn.Sequential(
            nn.Linear(self.dim_interaction + 1, 96),
            nn.ReLU(),
            nn.Linear(96, 1),
        )


    def forward(self, data):
        '''
          Forward pass
        '''
        temp = data.normalized_T
        data_dict = {
            'IL':   data.id_i,
            'solute':   data.id_s,
            'cation':   data.id_cation,
            'anion':   data.id_anion,
            'cationic_family':   data.id_cationic_family,
            'solute_family':   data.id_solute_family,
        }

        x_IL = self.IL_emb[0](data_dict['IL'])
        x_s = self.s_emb[0](data_dict['solute'])
        
        h = None
        if self.descriptors:
            x_cation = self.cation_emb[0](data_dict['cation'])
            x_anion = self.anion_emb[0](data_dict['anion'])
            x_cationic_family = self.cationic_family_emb[0](data_dict['cationic_family'])
            x_solute_family = self.solute_family_emb[0](data_dict['solute_family'])
            h = torch.cat([x_IL, x_s, x_cation, x_anion, x_cationic_family, x_solute_family], dim=1)
        else:
            h = torch.cat([x_IL, x_s], dim=1)
            
        
        h = self.layers_middel(h)

        h = torch.cat([h, temp.reshape(h.shape[0],1)], dim=1)

        y = self.layers_end(h)

        return y
         
