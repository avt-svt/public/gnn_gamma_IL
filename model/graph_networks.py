#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/gnn_gamma_IL
#
#*********************************************************************************

import torch
import torch.nn as nn
from torch.nn import Sequential, Linear, ReLU, GRU, BatchNorm1d
import torch.nn.functional as F
from torch_scatter import scatter_mean, scatter_add, scatter_max
import torch_geometric.transforms as T
from torch_geometric.nn import NNConv, GINEConv, Set2Set, GlobalAttention
from torch_geometric.nn.glob import global_mean_pool

def get_message_passing_layer(conv_type, node_dim, edge_dim, **kwargs):
    if conv_type == 'NNConv':
        edge_nn = Sequential(Linear(edge_dim, 128), nn.ReLU(), Linear(128, node_dim * node_dim))
        if False in [edge_nn]: raise ValueError(f'Some required argument not provided for message passing layer: {conv_type}')
        return NNConv(node_dim, node_dim, edge_nn, aggr='add')
    elif conv_type == 'GINEConv':
        gine_nn = Sequential(Linear(node_dim, int(node_dim*2)), nn.ReLU(), Linear(int(node_dim*2), node_dim))
        return GINEConv(gine_nn, train_eps=True)
    else:
        raise NotImplementedError(f'Conv type {conv_type} not implemented')


class GNN(nn.Module):

    def __init__(
        self, 
        num_node_features=22, 
        num_edge_features=6, 
        share_embedding=False, 
        conv_type='GINEConv',
        num_convs=1,
        use_gru=True,
        dim_fingerprint=64, 
        pool_type='add', 
        num_channel_layers=2,
        dim_channel=256, 
        interaction_function='concat',
        dim_interaction_layers=128,
        activation="LeakyReLU", 
        dropout=0.0, 
        **kwargs
    ):
        
        super().__init__()
        
        ### Hyperparameters ###
        # architecture
        self.share_embedding = share_embedding
        self.dim_fingerprint = dim_fingerprint
        self.conv_type = conv_type
        self.num_convs = num_convs
        self.use_gru = use_gru
        self.multi_gru = kwargs.get('multi_gru', False)
        if self.use_gru == False and self.multi_gru: raise ValueError('Error: you try use multiple GRUs but use of GRU is not active.')
        self.pool_type = pool_type
        # MLP-channels
        self.num_channel_layers = num_channel_layers
        self.dim_channel = dim_channel
        # MLP-prediction
        self.interaction_function = interaction_function
        self.dim_interaction_layers = dim_interaction_layers 
        self.dropout = dropout
        # Activation function
        if activation == "LeakyReLU":
            self.act_func = nn.LeakyReLU
        elif activation == 'ReLU':
            self.act_func = nn.ReLU
        elif activation == 'ELU':
            self.act_func = nn.ELU
        else:
            raise NotImplementedError(f'Activation function {activation} not implemented')
        # data features
        self.num_node_features = num_node_features
        self.num_edge_features = num_edge_features
        
        ### Graph convolutions ###
        # node (and edge) feature dimension adjustment
        self.lin0_i = torch.nn.Linear(self.num_node_features, self.dim_fingerprint)
        self.lin0_s = self.lin0_i if self.share_embedding else torch.nn.Linear(self.num_node_features, self.dim_fingerprint) 
        self.edge_transform_layer_i = None
        self.edge_transform_layer_s = None
        if self.conv_type in ['GINEConv']:
            self.edge_transform_layer_i = torch.nn.Linear(self.num_edge_features, self.dim_fingerprint)
            self.edge_transform_layer_s = self.edge_transform_layer_i if self.share_embedding else torch.nn.Linear(self.num_edge_features, self.dim_fingerprint) 

        # message passing layers
        self.conv_i = nn.ModuleList()
        self.conv_s = nn.ModuleList()
        self.gru_i = nn.ModuleList()
        self.gru_s = nn.ModuleList()
        for i in range(self.num_convs):
            nn_conv_i = get_message_passing_layer(conv_type=self.conv_type, node_dim=self.dim_fingerprint, edge_dim=self.num_edge_features)
            nn_conv_s = nn_conv_i if self.share_embedding else get_message_passing_layer(conv_type=self.conv_type, node_dim=self.dim_fingerprint, edge_dim=self.num_edge_features)
            self.conv_i.append(nn_conv_i)
            self.conv_s.append(nn_conv_s)
            if use_gru:
                gru_i = GRU(self.dim_fingerprint, self.dim_fingerprint)
                gru_s = gru_i if self.share_embedding else GRU(self.dim_fingerprint, self.dim_fingerprint)
                self.gru_i.append(gru_i)
                self.gru_s.append(gru_s)
                # check if multiple GRUs are to be used
                if not self.multi_gru:
                    break # if False: one GRU for multiple processing steps = num_convs
        
        # special pooling
        dim_channel_in = self.dim_fingerprint # vector dimension after pooling (does not change for add-, mean-, max-pooling)
        if 'set2set' in self.pool_type:
            # set2set layer
            processing_steps = kwargs.get('processing_steps', 3) # set2set has optional argument of processing steps
            self.set2set_i = Set2Set(dim_channel_in, processing_steps=processing_steps)
            self.set2set_s = self.set2set_i if self.share_embedding else Set2Set(dim_channel_in, processing_steps=processing_steps)
            # set2set out
            dim_channel_in = 2*dim_channel_in # vector dimension doubles through set2set pooling

        ## intermediate layer: message passing -> MLP-channels ##
        self.transfer_i = nn.Linear(dim_channel_in, self.dim_channel)
        self.transfer_s = self.transfer_i if self.share_embedding else nn.Linear(dim_channel_in, self.dim_channel)

        ### MLP-channels ###
        self.channel_i = nn.ModuleList()
        self.channel_s = nn.ModuleList()        
        self.drop = nn.Dropout(p=self.dropout)
        for i in range(self.num_channel_layers):
            channel_layer_i = nn.Linear(self.dim_channel, self.dim_channel)
            channel_layer_s = channel_layer_i if self.share_embedding else nn.Linear(self.dim_channel, self.dim_channel)
            self.channel_i.append(channel_layer_i)
            self.channel_s.append(channel_layer_s)
        self.channel_i.append(nn.Linear(self.dim_channel, self.dim_interaction_layers))
        self.channel_s.append(nn.Linear(self.dim_channel, self.dim_interaction_layers))

        ## interaction function (combine the two single-component embeddings): MLP-channesls -> MLP-prediction ##
        if self.interaction_function in ['concat', 'cat']:
            dim_interaction_in = self.dim_interaction_layers + self.dim_interaction_layers
        else: 
            raise NotImplementedError(f'Interaction function {interaction_function} not implemented')
            
        ### MLP-prediction ###
        # interaction MLP
        self.layers_middel = nn.Sequential(
            nn.Dropout(self.dropout),
            nn.Linear(dim_interaction_in, dim_interaction_in),
            self.act_func(),
            nn.Dropout(self.dropout),
            nn.Linear(dim_interaction_in, dim_interaction_in),
            self.act_func(),
            nn.Dropout(self.dropout),
        )
        # temperature-dependent MLP
        self.layers_end = nn.Sequential(
            nn.Linear(dim_interaction_in + 1, dim_interaction_in + 1),
            self.act_func(),
            nn.Linear(dim_interaction_in + 1, int(dim_interaction_in/2)),
            self.act_func(),
            nn.Linear(int(dim_interaction_in/2), 1),
        )

    def forward(self, data):
        '''
          Forward pass
        '''

        ### Get data ###
        x_i = data.x_i
        edge_index_i = data.edge_index_i
        edge_attr_i = data.edge_attr_i
        batch_i = data.x_i_batch

        x_s = data.x_s
        edge_index_s = data.edge_index_s
        edge_attr_s = data.edge_attr_s
        batch_s = data.x_s_batch

        temp = data.normalized_T

        ### Message passing & Pooling ###
        # IL message passing -> node embeddings
        out_i = self.act_func()(self.lin0_i(x_i))        
        if self.edge_transform_layer_i is not None: edge_attr_i = self.edge_transform_layer_i(edge_attr_i)
        h_i = out_i.unsqueeze(0)
        conv_l_idx = 0
        for i in range(self.num_convs):
            out_i = self.act_func()(self.conv_i[conv_l_idx](out_i, edge_index_i, edge_attr_i)) # convolutional layer
            if self.use_gru: out_i, h_i = self.gru_i[conv_l_idx](out_i.unsqueeze(0), h_i) # GRU
            out_i = out_i.squeeze(0)
            if (self.use_gru and self.multi_gru) or (not self.use_gru): conv_l_idx += 1
            
        # solute message passing -> node embeddings
        out_s = self.act_func()(self.lin0_s(x_s))
        if self.edge_transform_layer_s is not None: edge_attr_s = self.edge_transform_layer_s(edge_attr_s)
        h_s = out_s.unsqueeze(0)
        conv_l_idx = 0
        for i in range(self.num_convs):
            out_s = self.act_func()(self.conv_s[conv_l_idx](out_s, edge_index_s, edge_attr_s)) # convolutional layer
            if self.use_gru: out_s, h_s = self.gru_s[conv_l_idx](out_s.unsqueeze(0), h_s) # GRU
            out_s = out_s.squeeze(0)
            if (self.use_gru and self.multi_gru) or (not self.use_gru): conv_l_idx += 1

        # pooling -> graph embeddings
        x_forward_i = out_i # node embeddings of i
        x_forward_s = out_s # node embeddings of s
        if 'add' in self.pool_type:
            h_i = scatter_add(x_forward_i, batch_i, dim=0)
            h_s = scatter_add(x_forward_s, batch_s, dim=0)
        elif 'mean' in self.pool_type:
            h_i = scatter_mean(x_forward_i, batch_i, dim=0)
            h_s = scatter_mean(x_forward_s, batch_s, dim=0)
        elif 'max' in self.pool_type:
            h_i = scatter_max(x_forward_i, batch_i, dim=0)[0]
            h_s = scatter_max(x_forward_s, batch_s, dim=0)[0]
        elif 'set2set' in self.pool_type:
            h_i = self.set2set_i(x_forward_i, batch_i)
            h_s = self.set2set_s(x_forward_s, batch_s)

        ### intermediate layer: message passing & pooling -> MLP-channels ####
        h_i = self.act_func()(self.transfer_i(h_i))
        h_s = self.act_func()(self.transfer_s(h_s))
        
        ### MLP-channels ###
        for i in range(self.num_channel_layers + 1):
            h_i = self.drop(h_i)
            h_s = self.drop(h_s)
            h_i = self.act_func()(self.channel_i[i](h_i))
            h_s = self.act_func()(self.channel_s[i](h_s))

        ### interaction function (combine the two single-component embeddings): MLP-channesls -> MLP-prediction ###
        if self.interaction_function in ['concat', 'cat']:
            h = torch.cat([h_i, h_s], dim=1)
        
        ### MLP-prediction ###
        # interaction MLP
        h = self.layers_middel(h)
        # temperature-dependent MLP
        h = torch.cat([h, temp.reshape(h.shape[0],1)], dim=1)
        y = self.layers_end(h)
        
        #torch.set_printoptions(profile="full")

        return y

