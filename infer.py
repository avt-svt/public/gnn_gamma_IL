#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/gnn_gamma_IL
#
#*********************************************************************************

import os
import random
from copy import deepcopy
from datetime import datetime
from time import time
import numpy as np
import yaml
import argparse
import pandas as pd
import sklearn.metrics

import torch
import torch.nn as nn
import wandb
from torch_geometric.loader import DataLoader

from Data.graph_IL import GraphILDataset
from model.matrix_completion_networks import MCM_MLP
from model.graph_networks import GNN
from utils.split_dataset import split_train_vali
from utils.calculate_metrics import calculate_result_metrics

# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
   
    
def seed_everything(seed: int):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = True

def infer(config_path=None, dataloader=None, dataset=None):
    
    with open(config_path) as config_yaml:
    # use safe_load instead load
        config_dict = yaml.safe_load(config_yaml)
        for key, value in config_dict.items():
            if isinstance(value, dict):
                config_dict[key] = value

    if dataloader is None:
        # set seed to get same data split as in training
        seed_everything(config_dict["rs"])
        print(f"Set seed to: {config_dict['rs']}.")

        # load dataset
        print(f"\n---> Start loading of dataset {config_dict['data']} and data preprocessing.")
        if config_dict['data'] == "IL":
            data_set = GraphILDataset(f"./Data/IL/Train").shuffle()
            train_set, vali_set = split_train_vali(data_set, config_dict["split"])
            test_set = GraphILDataset(f"./Data/IL/Test").shuffle()
        elif config_dict['data'] == "IL_generalization":
            train_set = GraphILDataset(f"./Data/IL_generalization/Train_{config_dict['rs']}")
            vali_set = GraphILDataset(f"./Data/IL_generalization/Vali_{config_dict['rs']}")
            test_set = GraphILDataset(f"./Data/IL_generalization/Test")
        else:
            raise ValueError(f"Dataset {config_dict['data']} not available.")
            
        # Initialize data loaders
        if dataset == 'train': dataloader = DataLoader(train_set, batch_size=1024, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)
        if dataset == 'vali':  dataloader = DataLoader(vali_set, batch_size=1024, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)
        if dataset == 'test':  dataloader = DataLoader(test_set, batch_size=1024, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)
 
    print('---> Finished loading of dataset and data preprocessing.')
    
    print(f"\n---> Start init of model: {config_dict['model_id']}.")
    if config_dict["model_id"] in ["GraphNN", "GNN"]:
        model = GNN(**config_dict).to(device)
    elif 'MCM_MLP' in config_dict["model_id"]:
        data_set = GraphILDataset(f"./Data/IL/Train")
        model = MCM_MLP(data_set, **config_dict).to(device)
    
    model.load_state_dict(torch.load(config_dict["save_model_path"]))
    print(f"\tLoaded model parameters from {config_dict['save_model_path']}")
    print("---> Finished init of model.")

    def infer(loader):        
        model.eval()
        predictions = torch.tensor([], device=device)
        real_values = torch.tensor([], device=device)
        # get all predictions
        for data in loader:
            data = data.to(device)
            predictions = torch.cat((predictions, model(data).view(-1).detach()))
            real_values = torch.cat((real_values, data.y.detach()))
            
        return predictions, real_values
    
    # get and return predictions
    if dataset is None:
        predictions, _ = infer(dataloader)
        return predictions.cpu().detach().numpy(), None
    else:
        predictions, real_values = infer(dataloader)
        return predictions.cpu().detach().numpy(), real_values.cpu().detach().numpy()

    
def evaluate_ensemble(ensemble_paths, dataloader, df_data_set, df_target_col, dataset=None):
                                  
    
    analyze_ensemble = False
    # extract real values from df
    if dataloader is not None:
        analyze_ensemble = True
        real_values = df_data_set[df_target_col].to_numpy()
        
    # predictions from each ensemble
    all_predictions = []
    all_ln_result_metrics = {}
    all_result_metrics = {}
    
    # evaulate each model indivudally and store results
    for m_idx, m_p in enumerate(ensemble_paths):
        print(f"\nEvaulate model #: {m_p}")
        # infer 
        if dataloader is not None:
            predictions, _ = infer(config_path=m_p, dataloader=dataloader)
        elif dataset is not None:
            predictions, real_values = infer(config_path=m_p, dataloader=None, dataset=dataset)
        else: raise ValueError("No dataloader or dataset provided.")
        # store predictions
        if df_data_set is not None:
            df_data_set[f"predicted_lngamma_Model#{m_idx+1}"] = predictions
        # calculate result metrics
        ln_result_metric = calculate_result_metrics(y_true=real_values, y_pred=predictions)
        result_metric = calculate_result_metrics(y_true=np.exp(real_values), y_pred=np.exp(predictions))
        # store model output 
        all_predictions.append(predictions)
        all_ln_result_metrics[m_idx+1] = ln_result_metric
        all_result_metrics[m_idx+1] = result_metric
        
    # evaulate ensemble
    if analyze_ensemble:
        mean_predictions = np.mean(np.array(all_predictions), axis=0)
        df_data_set[f"predicted_lngamma_Ensemble"] = mean_predictions
        ensemble_ln_result_metric = calculate_result_metrics(y_true=real_values, y_pred=mean_predictions)
        ensemble_result_metric = calculate_result_metrics(y_true=np.exp(real_values), y_pred=np.exp(mean_predictions))
        # analyze ensemble effect
        for i in range(1,len(all_predictions)+1):
            i_mean_predictions = np.mean(np.array(all_predictions[:i]), axis=0)
            i_mae = sklearn.metrics.mean_absolute_error(y_true=real_values, y_pred=i_mean_predictions)
            print(f"# Models: {i}, MAE: {i_mae}")
    else: 
        ensemble_ln_result_metric = None
        ensemble_result_metric = None
        
    
    # get single model average metric
    def get_average_metric(metric_dict):
        average_metric = {}
        for k, _ in metric_dict[1].items():
            all_values  = [single_metric[k] for _, single_metric in metric_dict.items()]
            average_metric[k] = {
                "mean": np.mean(np.array(all_values)),
                "std": np.std(np.array(all_values)),
                "median": np.median(np.array(all_values))
            }
        return average_metric
            
    # calculate single model average metric
    average_ln_result_metric = get_average_metric(all_ln_result_metrics)
    average_result_metric = get_average_metric(all_result_metrics)
        
    return df_data_set, ensemble_ln_result_metric, ensemble_result_metric, average_ln_result_metric, average_result_metric

def write_metrics_to_txt(metric_dicts, path):
    with open(path, "w") as fp:
        for metric_name, metric_dict in metric_dicts.items():
            fp.write(f"\n\n### {metric_name} ###")
            for k, v in metric_dict.items():
                if isinstance(v, dict):
                    fp.write("{:<20s}".format(f"\n\t{k}:"))
                    for k_i, v_i in v.items():
                        if k_i == 'mean': 
                            fp.write("{:<20s}".format(f"{v_i:.7f}"))
                            fp.write("(")
                        else: fp.write(f"{k_i}: {v_i:.7f},")
                    fp.write(")")
                else:
                    fp.write("{:<20s}{:<20s}".format(f"\n\t{k}:", f"{v:.7f}"))

if __name__ == '__main__':
    
    data = 'IL'
    model_type = 'GNN'
    results_dir = f"results/{data}_{model_type}"
    ensemble_mode = True
    
    if data in 'IL':
        # train and val set
        df_train_val_set = pd.read_csv(f"./Data/{data}/Train/raw/raw.csv", sep=";", index_col="index")
        train_val_set = GraphILDataset(f"./Data/{data}/Train")
        train_val_loader = DataLoader(train_val_set, batch_size=1024, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)
        # test set
        df_test_data_set = pd.read_csv(f"./Data/{data}/Test/raw/raw.csv", sep=";", index_col="index")
        test_set = GraphILDataset(f"./Data/{data}/Test")
        test_loader = DataLoader(test_set, batch_size=1024, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)
    elif data == 'IL_generalization':
        # train and val set
        df_train_set = pd.read_csv(f"./Data/{data}/Train/raw/raw.csv", index_col="index", sep=";",)
        df_val_set = pd.read_csv(f"./Data/{data}/Vali/raw/raw.csv", index_col="index", sep=";",)
        df_train_val_set = pd.concat([df_train_set, df_val_set])
        df_train_val_set.to_csv(f"./Data/{data}/Train_val/raw/raw.csv", sep=";",)
        df_test_data_set = pd.read_csv(f"./Data/{data}/Test/raw/raw.csv", index_col="index", sep=";") 
        train_val_set = GraphILDataset(f"./Data/{data}/Train_val")
        test_set = GraphILDataset(f"./Data/{data}/Test")
        train_val_loader = DataLoader(train_val_set, batch_size=1024, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)
        test_loader = DataLoader(test_set, batch_size=1024, shuffle=False, follow_batch=['x_i', 'x_s'], num_workers=0, pin_memory=True)
    
    
    # paths for models in ensemble
    ensemble_paths = [f"./results/trained_models/{data}/{model_type}/{path}" for path in os.listdir(f"./results/trained_models/{data}/{model_type}/") if "config" in path]
    
    # evaluates ensemble models
    if ensemble_mode:
        # evaulate 
        
        df_train_val_set, ensemble_train_val_ln_result_metric, ensemble_train_val_result_metric, average_train_val_ln_result_metric, average_train_val_result_metric = evaluate_ensemble(ensemble_paths, train_val_loader, df_train_val_set, df_target_col="lngamma")
        
        df_train_val_set.to_csv(f"{results_dir}/train_val_set.csv")
        
        train_val_metric_dicts = {
            "Ensemble_ln_gamma_metric": ensemble_train_val_ln_result_metric,
            "Ensemble_gamma_metric": ensemble_train_val_result_metric,
        }
        write_metrics_to_txt(train_val_metric_dicts, f"{results_dir}/Train_val_metrics.txt")
        
        
        df_test_set, ensemble_test_ln_result_metric, ensemble_test_result_metric, average_test_ln_result_metric, average_test_result_metric = evaluate_ensemble(ensemble_paths, test_loader, df_test_data_set, df_target_col="lngamma")

        df_test_set.to_csv(f"{results_dir}/test_set.csv")

        test_metric_dicts = {
           "Ensemble_ln_gamma_metric": ensemble_test_ln_result_metric,
           "Ensemble_gamma_metric": ensemble_test_result_metric,
        }
        write_metrics_to_txt(test_metric_dicts, f"{results_dir}/Test_metrics.txt")
        
    # evaluate single models
    else:
        
        df_train_set, ensemble_train_ln_result_metric, ensemble_train_result_metric, average_train_ln_result_metric, average_train_result_metric = evaluate_ensemble(ensemble_paths=ensemble_paths, dataloader=None, df_data_set=None, df_target_col=None, dataset='train')

        train_metric_dicts = {
            "Average_ln_gamma_metric": average_train_ln_result_metric,
            "Average_gamma_metric": average_train_result_metric
        }
        write_metrics_to_txt(train_metric_dicts, f"{results_dir}/Train_metrics_single.txt")
        
        
        df_val_set, ensemble_val_ln_result_metric, ensemble_val_result_metric, average_val_ln_result_metric, average_val_result_metric = evaluate_ensemble(ensemble_paths=ensemble_paths, dataloader=None, df_data_set=None, df_target_col=None, dataset='vali')     

        val_metric_dicts = {
           "Average_ln_gamma_metric": average_val_ln_result_metric,
           "Average_gamma_metric": average_val_result_metric
        }
        write_metrics_to_txt(val_metric_dicts, f"{results_dir}/Val_metrics_single.txt")
        
        df_test_set, ensemble_test_ln_result_metric, ensemble_test_result_metric, average_test_ln_result_metric, average_test_result_metric = evaluate_ensemble(ensemble_paths=ensemble_paths, dataloader=None, df_data_set=None, df_target_col=None, dataset='test')
                
        test_metric_dicts = {
           "Average_ln_gamma_metric": average_test_ln_result_metric,
           "Average_gamma_metric": average_test_result_metric
        }
        write_metrics_to_txt(test_metric_dicts, f"{results_dir}/Test_metrics_single.txt")
        
