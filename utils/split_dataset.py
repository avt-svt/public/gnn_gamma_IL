#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/gnn_gamma_IL
#
#*********************************************************************************

from copy import deepcopy
import os
import pandas as pd


def generate_sets():
    
    if (os.path.exists("./Data/IL/Train/raw/raw.csv")) and (os.path.exists("./Data/IL/Test/raw/raw.csv")):
        return

    df = pd.read_csv('./Data/IL/data_IL.csv', sep=";")

    train_df, test_df = [x for _, x in df.groupby(df['split'] == 1)]

    if os.path.exists("./Data/IL/Train/raw/raw.csv") is False:
        train_df.to_csv(".Data/Train/raw/raw.csv", sep=';', encoding='utf-8')
    if os.path.exists("./Data/IL/Test/raw/raw.csv") is False:
        test_df.to_csv(".Data/Train/raw/raw.csv", sep=';', encoding='utf-8')


def generate_generalization_sets(seed=42, vali_fraction=0.05, test_fraction=0.05):
    
    if (os.path.exists("./Data/IL_generalization/Test/raw/raw.csv")) and (os.path.exists(f"./Data/IL_generalization/Train_{seed}/raw/raw.csv")) and (os.path.exists(f"./Data/IL_generalization/Train_{seed}/raw/raw.csv")):
        return                                                                                                                                          
    
    import rdkit
    from rdkit import Chem
    import random

    
    # fixed seed for test set!
    random.seed(42)

    # read in all data
    df = pd.read_csv('./Data/data_IL.csv', sep=";", index_col="index")
    original_num_datapoints = df.shape[0]
    # generate list of all smiles in data and ensure same smiles for same molecule with rdkit transformation
    all_smiles = df[["SMILES_IL", "SMILES_solute"]].values.ravel()
    all_unique_raw_smiles = pd.unique(all_smiles)
    print(f"\nLength all unique raw smiles: {len(all_unique_raw_smiles)}")
    all_unique_smiles = [Chem.MolToSmiles(Chem.MolFromSmiles(raw_smiles)) for raw_smiles in all_unique_raw_smiles]
    print(f"Length all unique smiles: {len(all_unique_smiles)}\n")
    if len(all_unique_raw_smiles) != len(all_unique_smiles): raise ValueError("Not all unique molecules have unique SMILES. Clean input data.")

    random.shuffle(all_unique_raw_smiles)

    len_test = int(len(all_unique_raw_smiles)*test_fraction)
    len_vali = int(len(all_unique_raw_smiles)*vali_fraction)
    test_smiles = all_unique_raw_smiles[-len_test:]

    print(f"Test SMILES: {test_smiles}\n")
    print(len(test_smiles))

    train_df = df[(~df["SMILES_IL"].isin(test_smiles)) & (~df["SMILES_solute"].isin(test_smiles))]
    test_df = df[(df["SMILES_IL"].isin(test_smiles)) | (df["SMILES_solute"].isin(test_smiles))]

    # only save once because test set is the same for all runs
    if os.path.exists("./Data/IL_generalization/Test/raw/raw.csv") is False:
        test_df.to_csv("./Data/IL_generalization/Test/raw/raw.csv", sep=';', encoding='utf-8')
    # otherwise check if smiles in already saved test set are the same as filtered now
    else:
        check_test_df = pd.read_csv('./Data/IL_generalization/Test/raw/raw.csv', sep=";", index_col="index")
        for index, row in check_test_df.iterrows():
            tmp_smiles_IL, tmp_smiles_solute = row["SMILES_IL"], row["SMILES_solute"]
            if (tmp_smiles_IL not in test_smiles) and (tmp_smiles_solute not in test_smiles):
                raise ValueError("Test smiles error!")

    all_smiles = train_df[["SMILES_IL", "SMILES_solute"]].values.ravel()
    all_unique_raw_smiles = pd.unique(all_smiles)
    print(f"\nLength all unique raw smiles in training set: {len(all_unique_raw_smiles)}")

    # shuffle rest with dynamic seed to get different validation set
    random.seed(seed)
    random.shuffle(all_unique_raw_smiles)

    vali_smiles = all_unique_raw_smiles[-len_vali:]

    print(f"\nValidation SMILES: {vali_smiles}")

    df = deepcopy(train_df)

    train_df = df[(~df["SMILES_IL"].isin(vali_smiles)) & (~df["SMILES_solute"].isin(vali_smiles))]
    vali_df = df[(df["SMILES_IL"].isin(vali_smiles)) | (df["SMILES_solute"].isin(vali_smiles))]

    os.makedirs(f"./Data/IL_generalization/Train_{seed}/raw/", exist_ok=True)
    os.makedirs(f"./Data/IL_generalization/Vali_{seed}/raw/", exist_ok=True)

    train_df.to_csv(f"./Data/IL_generalization/Train_{seed}/raw/raw.csv", sep=';', encoding='utf-8')
    vali_df.to_csv(f"./Data/IL_generalization/Vali_{seed}/raw/raw.csv", sep=';', encoding='utf-8')

    if original_num_datapoints != (train_df.shape[0] + vali_df.shape[0] + test_df.shape[0]): raise ValueError("Total length of new dataframe != length of original dataframe. Something went wrong.")

    print(f"\n--->Train length: {train_df.shape[0]}, Vali length: {vali_df.shape[0]}, Test length: {test_df.shape[0]}\n\n\n")
    
    
def split_train_vali(ds, split_i, num_splits=10):

    vali_perc = 1/num_splits
    data_len = len(ds)
    idxs = list(range(data_len))
    slice_len = data_len*vali_perc

    #idxs = indices
    vali_idxs = idxs[int(slice_len*split_i):int(slice_len*(split_i+1))]
    idxs[int(slice_len*split_i):int(slice_len*(split_i+1))] = []
    train_idxs = idxs
    
    # sanity check idxs
    for i in vali_idxs:
        if i in train_idxs: raise ValueError("Vali data in training set.")
    for i in train_idxs:
        if i in vali_idxs: raise ValueError("Training data in validation set.")

    # splitting data according to indices
    train_set = ds[train_idxs].copy()
    vali_set = ds[vali_idxs].copy()
    
    # sanity check length
    if len(train_set) + len(vali_set) != len(ds): raise ValueError("Number of data points in training + validation set does not equal raw dataset. Something went wrong during splitting the data.")

    return train_set, vali_set