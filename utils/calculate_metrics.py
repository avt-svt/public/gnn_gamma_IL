#**********************************************************************************
# Copyright (c) 2022 Process Systems Engineering (AVT.SVT), RWTH Aachen University
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
#
# The source code can be found here:
# https://git.rwth-aachen.de/avt-svt/public/gnn_gamma_IL
#
#*********************************************************************************

import sklearn.metrics

def calculate_result_metrics(y_true, y_pred):
        
        # calculate metric values
        mae = sklearn.metrics.mean_absolute_error(y_true, y_pred)
        mse = sklearn.metrics.mean_squared_error(y_true, y_pred)
        rmse = mse**(1/2)
        mape = sklearn.metrics.mean_absolute_percentage_error(y_true, y_pred)
        r2 = sklearn.metrics.r2_score(y_true, y_pred)
        maxe = sklearn.metrics.max_error(y_true, y_pred)
        expl_var = sklearn.metrics.explained_variance_score(y_true, y_pred)
        
        result_metric = {
            'mae': mae,
            'mse': mse,
            'rmse': rmse,
            'mape': mape,
            'r2': r2,
            'maxe': maxe,
            'expl_var': expl_var
        }
            
        return result_metric