# Graph neural networks for activity coefficients of IL solutions

This is the source code for the paper **[Graph neural networks for predicting temperature-dependent activity coefficients of solutes in ionic liquids](https://doi.org/10.1016/j.compchemeng.2023.108153)** (Computers & Chemical Engineering). 

## Overall framework structure

![Framework_structure](doc/images/GNN_gamma_IL_structure.png)

## Data & Usage

This repository contains following folders:

* **data**: acitvity coefficient dataset [1] for training the GNN and MCM [1]
* **doc**: documentation
* **models**: GNN and MCM [1] model structure
* **results**: results with trained models
* **utils**: util functions

### Data

The data set of infinite dilution activity coefficient was collected by Chen et al. [1] from the ILThermo database [2].

An overview of the complete data set and the training/validation/test sets can be found under the following paths:
* Complete data set: `Data/data_IL.csv`
* Prediction - training set: `Data/IL/Train/raw/raw.csv`
* Prediction - test set: `Data/IL/Train/raw/raw.csv`
* Generalization - training sets: `Data/IL_generalization/Train_<SEED>/raw/raw.csv`
* Generalization - validation sets: `Data/IL_generalization/Vali_<SEED>/raw/raw.csv`
* Generalization - test sets: `Data/IL_generalization/Test/raw/raw.csv`

### Usage

For **running** the GNN or MCM model, please install the required environment (see below) and then execute
`train.py` for training
or 
`infer.py` for inference/evaluation of the model against experimental data
or 
`predict.py` for making predicitons on a custom data set (see step-by-step guide below).

### Making predictions for custom data set

To make infinite dilution activity coefficient predictions for a given IL-solute pair at a given temperature, please follow the following steps:
* 1. Install all required packages 
* 2. Create a csv-file (e.g., `example.csv`) with following columns: `SMILES_IL`, `SMILES_solute`, `Temperature`
* 3. Run `predict.py` with the parser argument `--predict_data_path` corresponding to the path of the csv-file (e.g., `Data/Predict/example.csv`)

-> The predictions by the model will be printed and stored in the same folder as the custom file under **predictions.csv**.


References:
* [1]: Chen, G., Song, Z., Qi, Z., & Sundmacher, K. (2021). Neural recommender system for the activity coefficient prediction and UNIFAC model extension of ionic liquid‐solute systems. AIChE Journal, 67(4), e17171.
* [2]: Kazakov, et int. Frenkel, (2013). Ionic liquids database - ILThermo (v2.0), National Institute of Standards and Technology, Gaithersburg MD, 20899, [ILThermo](https://ilthermo.boulder.nist.gov), (accessed 14-06-2022).

## Required packages

The code is built upon: 

* **[PyTorch Geometric package](https://github.com/rusty1s/pytorch_geometric)**
* **[RDKit package](https://www.rdkit.org/)**

which need to be installed before using our code.

We recommend **setting up a conda environment** using the provided yaml-file:
`gnn4acil.yml `

## How to cite this work

Please cite [our paper](https://doi.org/10.1016/j.compchemeng.2023.108153) if you use this code:

This paper:

```
@article{Rittig.2023,
 author = {Rittig, Jan G. and {Ben Hicham}, Karim and Schweidtmann, Artur M. and Dahmen, Manuel and Mitsos, Alexander},
 year = {2023},
 title = {Graph neural networks for temperature-dependent activity coefficient prediction of solutes in ionic liquids},
 pages = {108153},
 volume = {171},
 issn = {00981354},
 journal = {Computers and Chemical Engineering},
 doi = {10.1016/j.compchemeng.2023.108153},
}
```

Please also refer to the corresponding packages, that we use, if appropiate:

Pytorch Geomteric:

```
@inproceedings{Fey/Lenssen/2019,
  title={Fast Graph Representation Learning with {PyTorch Geometric}},
  author={Fey, Matthias and Lenssen, Jan E.},
  booktitle={ICLR Workshop on Representation Learning on Graphs and Manifolds},
  year={2019},
}
```

RDKit:

```
@misc{rdkit,
 author = {{Greg Landrum}},
 title = {RDKit: Open-Source Cheminformatics},
 url = {http://www.rdkit.org}
}
```

