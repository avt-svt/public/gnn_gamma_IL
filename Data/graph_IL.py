from torch.utils.data import Dataset, DataLoader

import numpy as np
import pandas as pd
import time
import os
import torch
import torch.nn.functional as F
from torch_sparse import coalesce
from torch_geometric.data import (InMemoryDataset, download_url, extract_zip,
                                  Data)

from tqdm import tqdm
import rdkit
from rdkit import Chem
from rdkit.Chem.rdchem import HybridizationType
from rdkit.Chem.rdchem import BondType as BT



class PairData(Data):
    def __init__(self, x_i=None, edge_index_i=None, edge_attr_i=None, smiles_i=None, id_i=None,
                        x_s=None, edge_index_s=None, edge_attr_s=None, smiles_s=None, id_s=None,
                        id_cation=None, id_anion=None, id_cationic_family=None, id_solute_family=None,
                        normalized_T=None,
                        y=None,
                        index=None,
                        T=None

                    ):
        super().__init__()
        self.x_i = x_i
        self.x_s = x_s
        self.edge_index_i = edge_index_i
        self.edge_index_s = edge_index_s
        self.edge_attr_i = edge_attr_i
        self.edge_attr_s = edge_attr_s
        self.smiles_i = smiles_i
        self.smiles_s = smiles_s
        self.id_i = id_i
        self.id_s = id_s
        self.id_cation=id_cation
        self.id_anion=id_anion
        self.id_cationic_family=id_cationic_family
        self.id_solute_family=id_solute_family
        self.normalized_T = normalized_T
        self.y = y
        self.index = index
        self.T = T



    def __inc__(self, key, value, *args, **kwargs):
        if key == 'edge_index_i':
            return self.x_i.size(0)
        if key == 'edge_index_s':
            return self.x_s.size(0)
        else:
            return super().__inc__(key, value, *args, **kwargs)

class GraphILDataset(InMemoryDataset):
    r"""Activity Coefficient for Ionic Liquid and Solute Mixtures

    Args:
        root (string): Root directory where the dataset should be saved.
    """

    raw_url = ''
    processed_url = ''

    def __init__(self, root, transform=None, pre_iransform=None,
                 pre_filter=None):
        super(GraphILDataset, self).__init__(root, transform, pre_iransform, pre_filter)
        self.data, self.slices = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        return 'raw.pt' if rdkit is None else 'raw.csv'

    @property
    def processed_file_names(self):
        return 'data.pt'

    def download(self):
        pass

    def process(self):
        if rdkit is None:
            print('Using a pre-processed version of the dataset. Please '
                  'install `rdkit` to alternatively process the raw data.')

            self.data, self.slices = torch.load(self.raw_paths[0])
            data_list = [data for data in self]

            if self.pre_filter is not None:
                data_list = [d for d in data_list if self.pre_filter(d)]

            if self.pre_iransform is not None:
                data_list = [self.pre_iransform(d) for d in data_list]

            data, slices = self.collate(data_list)
            torch.save((data, slices), self.processed_paths[0])
            return
        cols = ['index', 'SMILES_IL', 'SMILES_solute', 'IL_id', 'solute_id', 'normalized_T', 'lngamma','T/K', 'cation_id', 'anion_id', 'cationic_family_id', 'solute_family_id_x']
        df = pd.read_csv(self.raw_paths[0], sep=";", usecols=cols)
        data_list = []
        for index, row in tqdm(df.iterrows(), total=df.shape[0]):
            #['SMILES_IL','SMILES_solute','lngamma']
            #print(row['SMILES_IL'], row['SMILES_solute'])
            x_i, edge_index_i, edge_attr_i, ascii_i, smiles_i = self._generate_features(row['SMILES_IL'])
            x_s, edge_index_s, edge_attr_s, ascii_s, smiles_s = self._generate_features(row['SMILES_solute'])
            id_i, id_s, cation_id, anion_id, cationic_family_id, solute_family_id = row['IL_id'], row['solute_id'], row['cation_id'], row['anion_id'], row['cationic_family_id'], row['solute_family_id_x']
            if x_s == None or x_i == None:
                continue

            data = PairData(
                x_i=x_i,
                x_s=x_s,
                edge_index_i=edge_index_i,
                edge_index_s=edge_index_s,
                edge_attr_i=edge_attr_i,
                edge_attr_s=edge_attr_s,
                smiles_i=smiles_i,
                smiles_s=smiles_s,
                id_i=id_i,
                id_s=id_s,
                id_cation=cation_id,
                id_anion=anion_id,
                id_cationic_family=cationic_family_id,
                id_solute_family=solute_family_id,
                normalized_T=row['normalized_T'],
                y=row['lngamma'],
                index=row['index'],
                T=row['T/K'],
            )

            #for same features for IL and solute
            data.num_node_features = data.x_i.shape[1]
            data.num_edge_features = data.edge_attr_i.shape[1]

            data_list.append(data)
            

        torch.save(self.collate(data_list), self.processed_paths[0])

    def _generate_features(self, smiles):
        if rdkit is None:
            print('Please install `rdkit` to process the raw data.')
            return None

        # special case for ClO4- (valence error, otherwise rdkit transforms mol to [O-][Cl+3]([O-])([O-])[O-])
        if "[O-]Cl(=O)(=O)=O" in smiles:
            mol = Chem.MolFromSmiles(smiles, sanitize=False)
            mol.UpdatePropertyCache(strict=False)
            Chem.SanitizeMol(mol,Chem.SanitizeFlags.SANITIZE_FINDRADICALS|Chem.SanitizeFlags.SANITIZE_KEKULIZE|Chem.SanitizeFlags.SANITIZE_SETAROMATICITY|Chem.SanitizeFlags.SANITIZE_SETCONJUGATION|Chem.SanitizeFlags.SANITIZE_SETHYBRIDIZATION|Chem.SanitizeFlags.SANITIZE_SYMMRINGS,catchErrors=True) 
        else:
            mol = Chem.rdmolfiles.MolFromSmiles(smiles)
            mol_block = Chem.MolToMolBlock(mol)
            mol = Chem.MolFromMolBlock(mol_block)
        if mol is None:
            print('Invalid molecule (None)')
            return None,None,None,None,None,

        N = mol.GetNumAtoms()


        types = {'C': 0, 'O': 1, 'N': 2, 'F': 3, 'S': 4,
                 'Cl': 5,'P': 6, 'B': 7, 'Br': 8}  # atom types
        bonds = {BT.SINGLE: 0, BT.DOUBLE: 1, BT.TRIPLE: 2, BT.AROMATIC: 3}  #bond types

        # atom features
        type_idx = []
        aromatic = []
        ring = []
        sp = []
        sp2 = []
        sp3 = []
        sp3d = []
        sp3d2 = []
        num_hs = []
        num_neighbors = []
        formal_charge_one_negative = []
        formal_charge_zero = []
        formal_charge_one_positive = []
        for atom in mol.GetAtoms():
            type_idx.append(types[atom.GetSymbol()])
            aromatic.append(1 if atom.GetIsAromatic() else 0)
            ring.append(1 if atom.IsInRing() else 0)
            hybridization = atom.GetHybridization()
            sp.append(1 if hybridization == HybridizationType.SP else 0)
            sp2.append(1 if hybridization == HybridizationType.SP2 else 0)
            sp3.append(1 if hybridization == HybridizationType.SP3 else 0)
            sp3d.append(1 if hybridization == HybridizationType.SP3D else 0)
            sp3d2.append(1 if hybridization == HybridizationType.SP3D2 else 0)
            if hybridization not in [HybridizationType.SP, HybridizationType.SP2,HybridizationType.SP3,HybridizationType.SP3D2]: raise ValueError("Invalid hybridization.")
            num_hs.append(atom.GetTotalNumHs(includeNeighbors=True))
            num_neighbors.append(len(atom.GetNeighbors()))
            formal_charge = atom.GetFormalCharge()
            if formal_charge not in [-1,0,1]: raise ValueError(f"Invalid formal charge {formal_charge} at atom {atom} for mol {Chem.MolToSmiles(mol)}")
            formal_charge_one_negative.append(1 if formal_charge == -1 else 0)
            formal_charge_zero.append(1 if formal_charge == 0 else 0)
            formal_charge_one_positive.append(1 if formal_charge == 1 else 0)

        x1 = F.one_hot(torch.tensor(type_idx), num_classes=len(types))
        x2 = torch.tensor([aromatic, ring, sp, sp2, sp3, sp3d2, formal_charge_one_negative, formal_charge_zero, formal_charge_one_positive], dtype=torch.float).t().contiguous()
        x3 = F.one_hot(torch.tensor(num_hs), num_classes=4)
        x = torch.cat([x1.to(torch.float), x2, x3.to(torch.float)], dim=-1)

        # bond features
        row, col, bond_idx, conj, ring = [], [], [], [], []
        for bond in mol.GetBonds():
            start, end = bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()
            row += [start, end]
            col += [end, start]
            bond_idx += 2 * [bonds[bond.GetBondType()]]
            conj.append(bond.GetIsConjugated())
            conj.append(bond.GetIsConjugated())
            ring.append(bond.IsInRing())
            ring.append(bond.IsInRing())

        if N > 1:
            edge_index = torch.tensor([row, col], dtype=torch.long)
            e1 = F.one_hot(torch.tensor(bond_idx),num_classes=len(bonds)).to(torch.float)
            e2 = torch.tensor([conj, ring], dtype=torch.float).t().contiguous()
            edge_attr = torch.cat([e1, e2], dim=-1)
        else:
            #initialize edge features to zero if edges do not exist
            edge_index = torch.tensor([[0], [0]], dtype=torch.long)
            num_edge_features = 6
            edge_attr = torch.zeros((1, num_edge_features))

        edge_index, edge_attr = coalesce(edge_index, edge_attr, N, N)



        # transform SMILES into ascii data type and store it in a name tensor
        name = str(Chem.MolToSmiles(mol))
        ascii_name = []
        for c in name:
            ascii_name.append(int(ord(c)))

        # if fails, increase range
        for i in range(len(ascii_name), 1000):
            ascii_name.append(0)

        ascii_name = torch.tensor([ascii_name], dtype=torch.float).contiguous()


        return x, edge_index, edge_attr, ascii_name, smiles